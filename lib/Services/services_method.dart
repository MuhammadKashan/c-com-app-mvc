import 'dart:convert';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

class ServicesMethod{

  getProducts() async {
    try{
      ///Get base url from .env file
      final String? baseUrl = dotenv.env["Base_Url"];

      ///Add end point into base url
      String url = '$baseUrl/products';

      ///make request
      final response = await http.get(Uri.parse(url));

      ///return body according to status code
      if(response.statusCode == 200){
        var decoder = jsonDecode(response.body);
        return decoder;
      }
      else if(response.statusCode == 201){
        var decoder = jsonDecode(response.body);
        return decoder;
      }
      else if(response.statusCode == 400){
        var decoder = jsonDecode(response.body);
        return decoder;
      }
      else if(response.statusCode == 404){
        var decoder = jsonDecode(response.body);
        return decoder;
      }
      else if(response.statusCode == 500){
        var decoder = jsonDecode(response.body);
        return decoder;
      }
    }
    catch(e){
      return e;
    }
  }

}