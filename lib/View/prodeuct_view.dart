import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import '../Controller/product_cont.dart';




// class ProductView extends StatelessWidget {
//   const ProductView({super.key});

class ProductView extends StatelessWidget {
  const ProductView({super.key});

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return
      GetBuilder<ProductProvider>(
          init: ProductProvider(),
          builder: (_) {
            return Scaffold(
                body:_.products.isEmpty?
                const Center(
                  child: SpinKitThreeBounce(
                    color: Color(0xffffb3b3),
                    size: 20.0,
                  ),
                ):
                ListView.builder(
                    itemCount: _.products.length,
                    itemBuilder: (context, index){
                      /// Extract the product image name from url
                      String url = _.products[index].image;
                      List<String> parts = url.split('/');
                      String lastPart = parts.last;

                      return ListTile(

                        /// Load image
                        leading: FutureBuilder(
                          future: Future.delayed(Duration(seconds: 2), () => '${_.baseUrl}/img/$lastPart'), // Simulating network delay
                          builder: (context, snapshot) {
                            if (snapshot.connectionState == ConnectionState.waiting) {
                              // Display a CircularProgressIndicator while loading
                              return const CircularProgressIndicator(
                                color: Color(0xffffb3b3),
                              );
                            } else if (snapshot.hasError) {
                              // Handle error if necessary
                              return Icon(Icons.error);
                            } else {
                              // Load the image when the future is resolved
                              return
                                // Image.network(snapshot.data.toString());
                                Image.network(
                                  '${snapshot.data}',
                                  errorBuilder: (context, error, stackTrace) {
                                    return Icon(Icons.error); // Display an error icon on image load failure
                                  },
                                  width: width * 0.2,
                                  height: height * 0.2,
                                );
                            }
                          },
                        ),

                        /// Navigation to product details
                        onTap: (){
                          _.navigateToNextPage(context, _.products[index], url, lastPart);
                        },

                        /// Product title
                        title: Text(_.products[index].title,
                          overflow: TextOverflow.ellipsis,
                        ),

                        /// Product description
                        subtitle: Text(_.products[index].description,
                          overflow: TextOverflow.ellipsis,
                        ),
                      );

                    })
            );
        }
    );

  }
}
