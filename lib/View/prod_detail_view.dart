import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ProductDetailView extends StatelessWidget {
  final data;
  const ProductDetailView({super.key, this.data});

  @override
  Widget build(BuildContext context) {
    print(data);
    // Object getStateObject = Provider.of<ProdDetailCont>(context, listen: false).parseUrl(data);
    print("indexedData: $data");
    return
      // Consumer<ProdDetailCont>(
      //     builder: (context, _, widget) {
      //       return Scaffold(
      //           body: Container()
      //       );
      //     }
      // );

      Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: CarouselSlider(
                items: [
                  FutureBuilder(
                    future: Future.delayed(Duration(seconds: 2), () => '${data.image}'), // Simulating network delay
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        // Display a CircularProgressIndicator while loading
                        return
                          const Center(
                            child: SpinKitThreeBounce(
                              color: Color(0xffffb3b3),
                              size: 20.0,
                            ),
                          );

                      } else if (snapshot.hasError) {
                        // Handle error if necessary
                        return const Icon(Icons.error);
                      } else {
                        // Load the image when the future is resolved
                        return
                          // Image.network(snapshot.data.toString());
                          Image.network(
                            '${snapshot.data}',
                            errorBuilder: (context, error, stackTrace) {
                              return Icon(Icons.error); // Display an error icon on image load failure
                            },
                            // width: width * 0.2,
                            // height: height * 0.2,
                          );
                      }
                    },
                  ),
                  // ...
                ],
                options: CarouselOptions(
                  autoPlay: true,
                  aspectRatio: 16/9,  // You can adjust the aspect ratio as needed
                  enlargeCenterPage: true,  // Makes the current item larger
                ),
              ),
            ),
            customContainer('${data.title}', 20.0, 20.0),
            customContainer('${data.category}', 16.0, 0.0),
            customContainer('\$${data.price.toStringAsFixed(2)}', 20.0, 0.0),
            customContainer('${data.description}', 16.0, 20.0),
          ],
        ),
      ),
    );
  }

  Widget customContainer(detail, font, top){
    return Container(
      padding: EdgeInsets.only(left: 20, right: 20, top: top),
      child: Text(
        '$detail',
        style: TextStyle(fontSize: font),
      ),
    );
  }
}
