import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get/get.dart';
import 'package:videcommerce/View/prod_detail_view.dart';
import '../Model/product_model.dart';
import '../Services/services_method.dart';

class ProductProvider extends GetxController{
  ServicesMethod serviceMethod = ServicesMethod();
  final String? baseUrl = dotenv.env["Base_Url"];
  List products = [];

  @override
  onReady(){
    productsList();
    super.onReady();
  }
  productsList()async{
    final productsData = await serviceMethod.getProducts();
    try {
      // Convert the data to a list of Product objects
      products = productsData.map((productData) {
        return
          Product.fromJson(productData);
        //   Product(
        //   id: productData['id'],
        //   title: productData['title'],
        //   price: productData['price'].toDouble(),
        //   description: productData['description'],
        //   category: productData['category'],
        //   image: productData['image'],
        // );
      }).toList();
      update();
    } catch (error) {
      // Handle any errors
      print('Error fetching products: $error');
    }
  }

  void navigateToNextPage(BuildContext context, indexProduct, indexUrl, indexImage) {
    // Your navigation logic here
    // You can use Navigator to navigate to the next page
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ProductDetailView(data: indexProduct)),
    );
  }
}